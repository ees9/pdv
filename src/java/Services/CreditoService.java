package Services;

import domain.Cliente;
import entities.Repository;
import java.math.BigDecimal;

public final class CreditoService {

    public static BigDecimal getCreditoUtilizado(Cliente cliente) {
        BigDecimal creditoUtilizado;
        creditoUtilizado = (BigDecimal) Repository
                .query("creditoUtilizado", cliente)
                .get(0);
        
        if (creditoUtilizado == null)
            creditoUtilizado = BigDecimal.ZERO;
        
        return creditoUtilizado;
    }
    
}
