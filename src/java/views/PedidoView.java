package views;

public class PedidoView {

    public static class RealizarPedidoView {

        public final static String TITULO = "Realizar Pedido";
        public final static String NOME = "Realizar Pedido";
        public final static String MEMBERS = "[Cabeçalho:12[#cliente,#data:2]; "
                + " Detalhe[adicionarItem(); "
                + " itens<#produto,#quantidade,*produto.preco,*total>;"
                + " *numeroDeItens]; "
                + " registrar()] ";

        public final static String NAMED_QUERY = "Select new domain.Pedido()";
    }

    public static class ConsultarPedidoView {

        public final static String TITULO = "Consultar Pedido";
        public final static String NOME = "Consultar Pedido";
        public final static String FILTROS = "*cliente, status";
        public final static String MEMBERS =  "Informações do Pedido"
                + "["
                + "[*id, *cliente; *data; *status];"
                + "[itens<*produto.nome,*quantidade,*produto.preco,*total>];"
                + "*total;[pagar(),cancelar()]"
                + "]";
        public final static int LINHAS = 1;
        public final static String TEMPLATE = "@FILTER+@PAGER";
        
        // NAMED QUERIES
        public final static String NAMED_QUERY_NOME =  "ConsultarPedido";
        
        // FIXME: Named Query temporária, não está ocorrendo o filtro pelo status, ajustar isso...
        public final static String NAMED_QUERY_TEMP =  "From domain.Pedido order by id";
        
        public final static String NAMED_QUERY = 
                "Select pedido From domain.Pedido pedido, domain.PedidoState status"
                + "Where pedido.status = status order by id";
    }
}
