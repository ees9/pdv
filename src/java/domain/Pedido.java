package domain;

import Services.CreditoService;
import entities.annotations.ActionDescriptor;
import entities.annotations.View;
import entities.annotations.Views;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Views({
    @View(
        title = "Realizar Pedido",
        name = "RealizarPedido",
        members = "[Cabeçalho[#cliente,#data:2]; "
            + " Detalhe[adicionarItem(); "
            + " itens<*produto,*quantidade,*produto.preco,*total>;"
            + " *numeroDeItens]; "
            + " registrar()] ",
        namedQuery = "Select new domain.Pedido()"
    ),
    
    @View(
        title = "Consultar Pedido",
        name = "ConsultarPedido",
        filters = "*cliente, data",
        namedQuery = "select p From Pedido p order by p.id ",
        members = "id,cliente,data,numeroDeItens,total,status",
        rows = 10,
        template = "@FILTER+@PAGER"
    )
})
@Entity
public class Pedido implements Serializable {

    static final double TOTAL_PEDIDO_MAXIMO = 1000d;
    
    static final int TOTAL_PEDIDO_ITENS_MINIMO = 1;

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(optional = false)
    private Cliente cliente;

    @Temporal(TemporalType.DATE)
    private Date data;

    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Item> itens;

    @ManyToOne(cascade = CascadeType.ALL)
    protected PedidoState status;

    @Column(precision = 8, scale = 2)
    private BigDecimal total;

    @Column(precision = 4)
    private Integer numeroDeItens = 0;

    public Pedido() {
        itens = new ArrayList<>();
        data = new Date();
        status = new NovoPedido();
    }
 
    
    public void adicionarItem(Produto produto, long quantidade) {
        Item item = new Item();
        item.setPedido(this);
        item.setProduto(produto);
        item.setQuantidade(quantidade);
        itens.add(item);
        numeroDeItens++;
    }

    boolean isTotalPedidoMaximoOk() {
        double totalPedido = this.getTotal().doubleValue();
        return totalPedido <= TOTAL_PEDIDO_MAXIMO;
    }
    
    boolean isTotalPedidoItensMinimoOk() {
        return itens.size() >= TOTAL_PEDIDO_ITENS_MINIMO;
    }

    boolean isLimiteCreditoOk() {
        BigDecimal saldoDevido = CreditoService.getCreditoUtilizado(cliente);
        BigDecimal totalPedido = this.getTotal();
        return saldoDevido.add(totalPedido).doubleValue() <= this.cliente.getLimiteCredito().doubleValue();
    }

    public BigDecimal getTotal() {
        total = BigDecimal.ZERO;

        itens.forEach((item) -> {
            total = total.add(item.getTotal());
        });

        return total;
    }

    @ActionDescriptor(
            confirm = true,
            confirmMessage = "Deseja registrar esse pedido ?",
            refreshView = true)
    public String registrar() {
        status
                .setPedido(this)
                .registrar();
        return "Pedido Registrado com sucesso, " + status.toString();
    }

    @ActionDescriptor(
            confirm = true,
            confirmMessage = "Deseja pagar esse pedido ?",            
            refreshView = true)
    public String pagar() {
        status
                .setPedido(this)
                .pagar();
        return "Pedido " + status.toString() + " com sucesso";
    }

    @ActionDescriptor(
            confirm = true,
            confirmMessage = 
                      "Deseja cancelar esse pedido ? \n"
                    + "ATENÇÃO!!! Esse operação não poderá ser desfeita.",
            refreshView = true)
    public String cancelar() {
        status
                .setPedido(this)
                .cancelar();
        return "Pedido " + status.toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public PedidoState getStatus() {
        return status;
    }

    void setStatus(PedidoState status) {
        this.status = status;
    }

    public Integer getNumeroDeItens() {
        return numeroDeItens;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
