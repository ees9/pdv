package domain;

import entities.annotations.EntityDescriptor;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@EntityDescriptor(hidden = true)
@Entity
@Table(name = "status")
public abstract class PedidoState implements Serializable {

    @Id
    protected Long id;
    
    @Transient
    protected Pedido pedido;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public PedidoState setPedido(Pedido pedido) {
        this.pedido = pedido;
        return this;
    }
    
    public abstract void registrar();
    
    public abstract void pagar();
    
    public abstract void cancelar();
}
