package domain;

import entities.annotations.EntityDescriptor;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@EntityDescriptor(hidden = true)
@Entity
public class Item implements Serializable {
    
    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(optional = false)
    private Pedido pedido;

    @ManyToOne(optional = false)
    protected Produto produto;

    @Column(precision = 10)
    private long quantidade;

    @Column( precision = 10, scale = 2)
    private BigDecimal total;

    public Item() {
        this.quantidade = 1;
    }
    
    public void remove() {
        pedido.getItens().remove(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getTotal() {
        total = produto.getPreco().multiply(new BigDecimal(quantidade));
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

//    public BigDecimal getTotal() {
//           
//        total = produto.getPreco().multiply(new BigDecimal(this.quantidade));
//        return total;
//    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Item other = (Item) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return quantidade + " - " +produto.getNome();
    }
}
