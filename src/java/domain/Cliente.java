package domain;

import entities.annotations.View;
import entities.annotations.Views;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Views ({
    @View(name = "Cliente",
            title = "Cliente",
            filters = "id; nome; cnpj; cidade",
            members = "Clientes[id; nome; cnpj; cidade; limiteCredito]",
            namedQuery = "From domain.Cliente order by nome",
            rows = 5,
            template = "@CRUD_PAGE+@FILTER"
    )
})
@Entity
@NamedQueries({
    @NamedQuery(
        name = "creditoUtilizado",
        query = "select sum(p.total) from Pedido p, AguardandoPagamento st "
              + "where p.status = st "
              + "  and p.cliente = :cliente "
    )
})
public class Cliente implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(length = 40, nullable = false)
    private String nome;

    @Column(length = 14)
    private String cnpj;

    @Column(length = 20)
    private String cidade;

    @Column(name = "limite_credito", precision = 10, scale = 2)
    private BigDecimal limiteCredito;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public BigDecimal getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(BigDecimal limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Cliente other = (Cliente) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }

}
