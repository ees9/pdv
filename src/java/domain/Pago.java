package domain;

import entities.annotations.EntityDescriptor;
import javax.persistence.Entity;

@EntityDescriptor(hidden = true)
@Entity
public class Pago extends PedidoState {

    public Pago() {
        this.id = 3L;
    }

    @Override
    public void registrar() {
        throw new IllegalStateException("Não é possível registrar um pedido pago.");
    }

    @Override
    public void pagar() {
        throw new IllegalStateException("Não é possível pagar um pedido pago.");
    }

    @Override
    public void cancelar() {
        throw new IllegalStateException("Não é possível cancelar um pedido pago.");
    }

    @Override
    public String toString() {
        return "Pago";
    }

}
