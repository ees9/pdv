package domain;

import javax.persistence.Entity;

import entities.Repository;
import entities.annotations.EntityDescriptor;

@EntityDescriptor(hidden = true)
@Entity
public class AguardandoPagamento extends PedidoState {

    public AguardandoPagamento() {
        this.id = 2L;
    }

    @Override
    public void registrar() {
        throw new IllegalStateException("Não é possível registrar um pedido registrado.");
    }

    @Override
    public void pagar() {
        this.pedido.setStatus(new Pago());
        Repository.save(pedido);
    }

    @Override
    public void cancelar() {
        this.pedido.setStatus(new Cancelado());
        Repository.save(pedido);
    }

    @Override
    public String toString() {
        return "Aguardando Pagamento";
    }

}
