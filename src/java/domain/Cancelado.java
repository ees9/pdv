package domain;

import entities.annotations.EntityDescriptor;
import javax.persistence.Entity;

@EntityDescriptor(hidden = true)
@Entity
public class Cancelado extends PedidoState {

    public Cancelado() {
        this.id = 4L;
    }

    @Override
    public void registrar() {
        throw new IllegalStateException("Não é possível registrar um pedido cancelado.");
    }

    @Override
    public void pagar() {
        throw new IllegalStateException("Não é possível pagar um pedido cancelado.");
    }

    @Override
    public void cancelar() {
        throw new IllegalStateException("Não é possível cancelar um pedido cancelado.");
    }

    @Override
    public String toString() {
        return "Cancelado";
    }
}
