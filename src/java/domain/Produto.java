package domain;

import entities.annotations.View;
import entities.annotations.Views;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Views ({
    @View(name = "Produto",
            title = "Produto",
            filters = "id; nome",
            members = "Produtos[id; nome; preco]",
            namedQuery = "From domain.Produto order by nome",
            rows = 5,
            template = "@CRUD_PAGE+@FILTER"
    )
})
@Entity
public class Produto implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(length = 20, nullable = false)
    private String nome;

    @Column(precision = 10, scale = 2)
    private BigDecimal preco;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Produto other = (Produto) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }
}
