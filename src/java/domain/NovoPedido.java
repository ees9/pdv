package domain;

import javax.persistence.Entity;

import entities.Repository;
import entities.annotations.EntityDescriptor;

@EntityDescriptor(hidden = true)
@Entity
public class NovoPedido extends PedidoState {

    public NovoPedido() {
        this.id = 1L;
    }

    @Override
    public void registrar() {
        if (!this.pedido.isTotalPedidoMaximoOk())
            throw new IllegalStateException("Total do pedido excede o total máximo permitido R$ "
                    + String.valueOf(Pedido.TOTAL_PEDIDO_MAXIMO));
        
        if (!this.pedido.isTotalPedidoItensMinimoOk()) 
            throw new IllegalStateException("O Pedido deve ter no mínimo "
                    + String.valueOf(Pedido.TOTAL_PEDIDO_ITENS_MINIMO)
                    + " item(s)");

        if (!this.pedido.isLimiteCreditoOk())
            throw new IllegalStateException("Limite de crédito excedido");

        pedido.setStatus(new AguardandoPagamento());
        Repository.save(pedido);
    }

    @Override
    public void pagar() {
        throw new IllegalStateException("Não é possível pagar um pedido não registrado.");
    }

    @Override
    public void cancelar() {
        throw new IllegalStateException("Não é possível cancelar um pedido não registrado.");
    }

    @Override
    public String toString() {
        return "Novo Pedido";
    }

}
